# Wienimal Admin Toolbar

## Admin logo

The module will automatically load the `admin-logo.svg` from the default theme. If no logo is provided, a fallback logo from the module is provided.

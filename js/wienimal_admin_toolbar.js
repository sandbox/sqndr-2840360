(function() {
    /**
     * Force the Drupal toolbar to be vertical.
     */
    localStorage.setItem('Drupal.toolbar.trayVerticalLocked', true);
})();
